### **DEKORATOR (wzorzec projektowy)**

**Dekorator** – wzorzec projektowy należący do grupy wzorców strukturalnych. Służy on do dekorowania, czyli dodania istniejącej klasie, nowe zachowanie, ale nie zmienia działania klasy podstawowej. Czyli spełnia dwie pierwsze zasady solid, zasadę pojedynczej odpowiedzialności(Single responsibility) i zasadę otwarty zamknięty(open-closed). Zwykle przekazuje się oryginalny obiekt jako parametr konstruktora dekoratora, metody dekoratora wywołują metody oryginalnego obiektu i dodatkowo implementują nową funkcję.

Dekoratory są alternatywą dla dziedziczenia. Dziedziczenie rozszerza zachowanie klasy w trakcie kompilacji, w przeciwieństwie do dekoratorów, które rozszerzają klasy w czasie działania programu.

#### **Przykład zastosowania:**
Rozważmy okno w graficznym interfejsie użytkownika. By pozwolić na przewijanie jego zawartości, należy dodać do niego poziome lub pionowe paski przewijania. Okna są reprezentowane przez instancję klasy Okno i klasa ta nie powinna posiadać żadnych metod dodających paski przewijania. Można utworzyć podklasę PrzewijaneOkno, która udostępniałaby te metody lub stworzyć OknoDekoratorPrzewijane, który jedynie dodawałby tę funkcję do istniejących obiektów Okno. W tym miejscu działałyby oba rozwiązania.

Teraz załóżmy, że potrzeba dodać ramki dookoła okien. I w tym przypadku klasa Okno nie ma takiej funkcji. Pojawia się problem z podklasą PrzewijaneOkno, bo aby dodać ramki do wszystkich okien potrzeba stworzyć podklasy OknoZRamką i OknoPrzewijaneZRamką. Problem staje się jeszcze większy z każdą kolejną opcją. W przypadku dekoratorów wystarczy stworzyć jedną klasę OknoDekoratorRamka - podczas działania programu można dynamicznie dekorować istniejące okna z OknoDekoratorPrzewijane lub OknoDekoratorRamka lub oboma.

Przykładem zastosowania wzorca dekoratora jest implementacja strumieni I/O w Javie.

#### **Struktura wzorca**
![struktura](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Decorator_classes_pl.svg/1280px-Decorator_classes_pl.svg.png)


#### **Konsekwencje stosowania**
**Zalety:**

1. Zapewnia większą elastyczność niż statyczne dziedziczenie

2. Pozwala uniknąć tworzenia przeładowanych funkcjami klas na wysokich poziomach hierarchii - rozbicie funkcjonalności

**Wady:**

1. Projekty, w ktorych jest używany, charakteryzują się mnogością małych podobnych do siebie obiektow, ktore rożnią się jedynie tym jak są ze sobą połączone. Poznanie i zrozumienie ich w celu modyfikacji może być przez to mocno utrudnione.

2. Nie można także korzystać w takich systemach z identyczności obiektow. Dekorator działa jak przezroczysta otoczka, ale z punktu widzenia identyczności obiekt udekorowany nie jest taki sam jak wejściowy.

**Źródła:**

http://devman.pl/pl/techniki/wzorce-projektowe-7-dekoratordecorator/
https://pl.wikipedia.org/wiki/Dekorator_(wzorzec_projektowy)
